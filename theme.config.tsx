import React from 'react';
import { DocsThemeConfig } from 'nextra-theme-docs';

const config: DocsThemeConfig = {
  logo: <span>Developer roadmap</span>,
  docsRepositoryBase: 'https://gitlab.com/huyxvd/developer-roadmap/-/tree/master/',
  footer: {
    text: 'Developer roadmap',
  },

  useNextSeoProps: () => {
    return {
      titleTemplate: '%s – Culi'
    }
  }
}

export default config
